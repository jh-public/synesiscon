'use strict';

angular.module( 'app' )

.component( 'underConstruction', {
    templateUrl: './components/under-construction/index.html'
    ,controller: [ '$stateParams', function( $stateParams ) {
        this.$onInit = function(){
            this.title = $stateParams.title;
        }
    }]
})

'use strict';
angular.module( 'app', [ 'ui.router' ] )

.config([
         '$stateProvider'
        ,'$urlRouterProvider'
    ,function(
         $stateProvider
        ,$urlRouterProvider
    ){

    var states = [
        {
            name: 'dashboard'
            ,url: '/dashboard'
            ,component: 'underConstruction'
            ,params: {
                title: 'Dashboard'
            }
        }
        ,{
            name: 'myCars'
            ,url: '/my-cars'
            ,component: 'underConstruction'
            ,params: {
                title: 'My Cars'
            }
        }
        ,{
            name: 'addCar'
            ,url: '/add-car'
            ,component: 'underConstruction'
            ,params: {
                title: 'Add Car'
            }
        }
        ,{
            name: 'booking'
            ,url: '/booking'
            ,component: 'underConstruction'
            ,params: {
                title: 'Booking'
            }
        }
        ,{
            name: 'idProofs'
            ,url: '/id-proofs'
            ,component: 'idProofs'
        }
        ,{
            name: 'profile'
            ,url: '/profile'
            ,component: 'underConstruction'
            ,params: {
                title: 'Profile'
            }
        }
        ,{
            name: 'logout'
            ,url: '/logout'
            ,component: 'underConstruction'
            ,params: {
                title: 'Logout'
            }
        }
        ,{
            name: 'services'
            ,url: '/services'
            ,component: 'underConstruction'
            ,params: {
                title: 'Services'
            }
        }
        ,{
            name: 'myAccount'
            ,url: '/my-account'
            ,component: 'underConstruction'
            ,params: {
                title: 'My Account'
            }
        }
        ,{
            name: 'contactUs'
            ,url: '/contact-us'
            ,component: 'underConstruction'
            ,params: {
                title: 'Contact Us'
            }
        }
    ];

    states.map( function( state ){
        $stateProvider.state( state );
    });

    $urlRouterProvider.otherwise( '/id-proofs' );
}])

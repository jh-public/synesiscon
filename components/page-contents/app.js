'use strict';

angular.module( 'app' )

.component( 'pageContents', {
    templateUrl: './components/page-contents/index.html'
})

'use strict';

angular.module( 'app' )

.component( 'idProofs', {
    templateUrl: './components/id-proofs/index.html'
    ,controller: [ function(){

        this.$onInit = function(){
            this.items = this.getItems( 12 );
        }

        this.addId = function( itemNo ){
            alert( 'Add ID button ' + itemNo + ' clicked.\n\nUnder construction.' );
        }

        this.addImage = function(){
            alert( 'Add image button clicked.\n\nUnder construction.' );
        }

        this.getItems = function( count ){
            var items = [];
            for ( var i=0; i<count; i++ ){
                items.push({
                    value: "Will Smith / Voter-ID"
                });
            }
            return items;
        }

    }]
})
